import React, { Component } from 'react';
import {Grid,Row,Col} from 'react-bootstrap';


class Component1 extends React.Component {
   render() {
      return (
         <div>
           <Grid className="text-center">
            <h1>Component1 Page inside page</h1>
            <p> This is about us page</p>
            </Grid>
         </div>
      )
   }
}

export default Component1;
