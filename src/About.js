import React, { Component } from 'react';
import {Grid,Row,Col} from 'react-bootstrap';
import Component1 from './Component1';


class About extends React.Component {
   render() {
      return (
         <div>
           <h1 className="text-center"> This is the About Page with some Random Contents</h1>
           <Grid>
             <Row>
               <Col md={6} className="text-center">
                 <h1> Content 1</h1>
                 <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                  when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                  It has survived not only five centuries, but also the leap into electronic typesetting,
                  remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                  sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                  Aldus PageMaker including versions of Lorem Ipsum.
                 </p>

               </Col>
               <Col md={6} className="text-center">
                 <h1> Content 1</h1>
                 <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                  when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                  It has survived not only five centuries, but also the leap into electronic typesetting,
                  remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                  sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                  Aldus PageMaker including versions of Lorem Ipsum.
                 </p>
               </Col>
             </Row>
             <div>
               <Component1/>
             </div>
           </Grid>

         </div>
      )
   }
}

export default About;
