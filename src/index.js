import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  IndexRoute,
  Route,
  hashHistory
} from 'react-router-dom';
import App from './App';
import Home from './Home';
import About from './About';

ReactDOM.render(
<App/>,
  document.getElementById('root')
);
