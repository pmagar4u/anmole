import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  IndexRoute,
  Route,
  Link
} from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import {Grid,Row,Col} from 'react-bootstrap';

class Footer extends React.Component{
  render() {
     return (
          <div>
<Router>
  <div>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="/about" component={About}/>
    </Route>
  </div>

            <Grid>
              <Row>
                <Col md={6} className="text-center">
                  <ul className="list-inline">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/about">About</Link></li>
                  </ul>
              </Col>
              <Col md={6} className="text-center">
                <ul className="list-inline">
                  <li><Link to="/https:www.facebook.com">facebook</Link></li>
                  <li><Link to="/https:www.twitter.com">twitter</Link></li>
                  <li><Link to="/https:www.instagram.com">instagram</Link></li>
                </ul>
              </Col>
              </Row>
            </Grid>
</Router>
          </div>
        )
     }
  }


export default Footer;
